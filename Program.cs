﻿using System;
using System.Threading;
using System.Collections;
using System.Collections.Generic;

namespace threads
{
    class Program
    {

        public static Dictionary<string,string> dic = new Dictionary<string, string>();
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");

            Thread t1 = new Thread(func);
            Thread t2 = new Thread(func);

            t1.Start();
            //Thread.Sleep(1);
            t2.Start();

            Console.ReadKey();
        }

        static void func()
        {
            Console.Write("a\n");
            try{

                lock(dic){

            if (!dic.ContainsKey("a"))
              Program.dic.Add("a", "bb");
                }
            }
            catch (Exception e)
            {
                Console.Write(e);
            }
        }
    }
}
